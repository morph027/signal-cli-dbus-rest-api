# signal-cli DBus REST API

{{% notice info %}}
**This application has not been audited. It should not be regarded as
secure, use at your own risk. This is a third-party effort, and is NOT a part of the official Signal
project or any other project of Open Whisper Systems.**
{{% /notice %}}

This project wraps [AsamK/signal-cli](https://github.com/AsamK/signal-cli/) DBus service to provide a REST API. This allows other apps (reporting, monitoring, ...) to send private and encrypted Signal messages instead of e-mail or other proprietary and non-free protocols.

It is quite similar to [bbernhard/signal-cli-rest-api](https://github.com/bbernhard/signal-cli-rest-api/), which provides a dockerized AIO gateway using signal-cli JSON-RPC. This project differs as i wanted a solution to be usable w/o docker too. I started this project as successor to my previous "Signal Web Gateway", but i'm tempted to mimic bbernhard's API routes as close as possible. Mine does not provide as much features yet, but the existing ones are API compatible.

{{% notice info %}}
You will need a spare phone number to use for registration! (SIP numbers are fine)
{{% /notice %}}

{{% notice warning %}}
**Security notice**:
You should run the gateway behind a reverse proxy like nginx to add tls/basic auth/acls.
{{% /notice %}}
