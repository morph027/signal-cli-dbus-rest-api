+++
title = "Addons"
weight = 4
+++

Ideas for tiny extensions based on this project:

* [Mail2Signal](/signal-web-gateway/addons/mail2signal/)
* [Interactive Bot](/signal-web-gateway/addons/bot/)
