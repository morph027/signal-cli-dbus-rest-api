+++
title = "Configuration"
weight = 2
+++

{{< tabs groupId="configuration" >}}
{{% tab name="signal-cli-dbus-rest-api" %}}
Variable | Default | Description
---|---|---|
`SIGNAL_CLI_DBUS_REST_API_HOST` | `127.0.0.1` | Address to host the server on.
`SIGNAL_CLI_DBUS_REST_API_PORT` | `8080` | Port to host the server on.
`SIGNAL_CLI_DBUS_REST_API_DEBUG` | `False` | Enables debug output (slows server).
`SIGNAL_CLI_DBUS_REST_API_WORKERS` | `1` | Number of worker processes to spawn.
`SIGNAL_CLI_DBUS_REST_API_ACCESS_LOG` | `False` | Enables log on handling requests (significantly slows server).
`SIGNAL_CLI_DBUS_REST_API_ACCOUNT` | n/a | Used for `/v1/send` without specifying a number in your POST request.
{{% /tab %}}
{{% tab name="signal-cli" %}}
Variable | Default | Description
---|---|---|
`SIGNAL_CLI_PARAMS` | `--config /var/lib/signal-cli` | Parameter for `signal-cli` command
`SIGNAL_CLI_DAEMON_PARAMS` | `--system --no-receive-stdout` | Parameters for `signal-cli daemon` subcommand
{{% /tab %}}
{{< /tabs >}}
