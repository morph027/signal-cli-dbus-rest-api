+++
title = "Installation"
weight = 1
+++

* [Docker](/signal-cli-dbus-rest-api/installation/docker/)
* [Standalone](/signal-cli-dbus-rest-api/installation/standalone/)
