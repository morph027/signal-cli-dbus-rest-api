+++
title = "Docker"
+++


## Run container

{{< tabs groupId="run" >}}
{{% tab name="Docker" %}}
```bash
docker run -d \
  --name signal-cli-dbus-rest-api \
  --env SIGNAL_CLI_DBUS_REST_API_HOST=0.0.0.0 \
  --publish 8080:8080 \
  --volume /some/local/dir/signal-cli-config:/var/lib/signal-cli \
  --tmpfs /run \
  --tmpfs /tmp:exec \
  registry.gitlab.com/morph027/signal-cli-dbus-rest-api/signal-cli-dbus-rest-api:latest
```
If you like to use `/v1/send` without specifying a number in your POST request, you'll need to add
`--env SIGNAL_CLI_DBUS_REST_API_ACCOUNT=+XX`, where XX is the number registered with signal-cli.
{{% /tab %}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli-dbus-rest-api:
    image: registry.gitlab.com/morph027/signal-cli-dbus-rest-api/signal-cli-dbus-rest-api:latest
    environment:
      - SIGNAL_CLI_DBUS_REST_API_HOST=0.0.0.0
      # - SIGNAL_CLI_DBUS_REST_API_ACCOUNT=+XX # If you like to use `/v1/send` without specifying a number in your POST request
    ports:
      - "8080:8080"
    volumes:
      - "/some/local/dir/signal-cli-config:/var/lib/signal-cli"
    tmpfs:
      - "/run"
      - "/tmp:exec"
```
{{% /tab %}}
{{< /tabs >}}

## Configuration

See [Configuration]({{< ref "/configuration" >}} "Configuration")
