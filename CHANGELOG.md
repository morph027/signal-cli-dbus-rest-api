# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased

## 22.2.3 - 2022-02-03
## Changes
- increase minimum python version to 3.8 (`importlib.metadata` missing in 3.7)

## 22.2.2 - 2022-02-02
## Changes
- include swagger assets (css, js) into build

## 22.2.1 - 2022-02-02 - REVOKED
### Changes
- include swagger assets (css, js) into build
